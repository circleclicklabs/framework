all:
	python3 -m web

clean:
	find . -name __pycache__ | xargs rm -fr
	find . -name '*~' | xargs rm -fr
