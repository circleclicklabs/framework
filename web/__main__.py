#!/usr/bin/env python3
from gevent.monkey import patch_all as _;_()
import bottle

import sqlite3

connection = None

def connect():
    global connection
    if not connection:
        connection = sqlite3.connect('tmp.db')
        pass
    return connection

def create_xyz(cursor=None):
    cursor = cursor or connect().cursor()
    cursor.execute("create table xyz (integer integer)")
    cursor.execute("insert into xyz values (1), (2)")
    return cursor

def read_xyz(cursor=None):
    cursor = cursor or connect().cursor()
    cursor.execute("select * from xyz")
    return cursor       

@bottle.get('/create')
def _():
    create_xyz()
    return 'hm'

@bottle.get('/read')
def _():
    return 'READ'
    arr = []
    for row in read_xyz():
        print("R", row)
        arr.append(row)
        pass
    return f'''
here's the array: {arr}
'''

@bottle.get('/')
@bottle.get('/<path>')
def _(path='index.html'):
    return bottle.static_file(path, 'static')

if __name__=='__main__':
    bottle.run(
        host='', port=7777,
        debug=True, reloader=True,
        server='gevent')
